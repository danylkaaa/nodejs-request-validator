# config

[![pipeline status](https://gitlab.com/ZulusK/nodejs-request-validator/badges/master/pipeline.svg)](https://gitlab.com/ZulusK/nodejs-request-validator/commits/master)
[![coverage report](https://gitlab.com/ZulusK/nodejs-request-validator/badges/master/coverage.svg)](https://gitlab.com/ZulusK/nodejs-request-validator/commits/master)

Validator for configuration files, based on Joi, loads configuration, based on `process.env.NODE_ENV` property

```bash
npm i @zulus/request-validator
```

### Structure

- [API](#api)
- [Usage](#usage)
- [Contributing rules](#contributing)

<a name="usage"></a>

### USAGE
```js
const { middleware }=require('@zulus/request-validator')
const Joi = require('Joi');
const schema = Joi.object({
  //...
});
router.get('/route', middleware(schema), handler);
```

<a name="api"></a>

### API
### `middleware (shema, options)`
Returns Koa middleware for validation request object, using Joi schema
* `schema` - valid Joi schema for validation
* `[options]` - configuration options for Joi.validate method call
    * `[allowUnknown]` - allows object to contain unknown keys which are ignored
    * `[skipFunctions]` - event is not supposed to have functions
    * `[stripUnknown]` - remove unknown elements from objects and arrays, if you need this behavior use .unknown()
    * `[presence]` - sets the default presence requirements: 'optional', 'required', or 'forbidden'
    * `[abortEarly]` - when true, stops validation on the first error, otherwise returns all the errors found. Defaults to true  

### `validate (schema, [options],[errorConverter])`
Returns result object:
```js
{
  ok: boolean, // is object passed validation
  errors: {
    path:string // error description 
  },
  value: object // validated values
}
```
* `schema` - valid Joi schema for validation
* `[options]` - configuration options for Joi.validate method call
    * `[allowUnknown]` - allows object to contain unknown keys which are ignored
    * `[skipFunctions]` - event is not supposed to have functions
    * `[stripUnknown]` - remove unknown elements from objects and arrays, if you need this behavior use .unknown()
    * `[presence]` - sets the default presence requirements: 'optional', 'required', or 'forbidden'
    * `[abortEarly]` - when true, stops validation on the first error, otherwise returns all the errors found. Defaults to true 
* `[errorConverter]` - function for converting validation error to an API error
<a name="contributing"></a>

### Contributing

To start contributing do

```bash
git clone git@gitlab.com:ZulusK/nodejs-request-validator.git
git checkout develop
git checkout -b <your-branch-name>
```

The project is developed in accordance with the [GitFlow][gitflow-docs] methodology.

##### What it means

1. All work you should do in your own **local** branch (naming is important, look below), then make pull request to **develop** branch
2. Your local branch should not have conflicts with repository **develop** branch. To avoid it, before push to repository, do:
   ```bash
   git pull origin develop
   # resolve all conflicts, if they exists
   git add --all
   git commit -m "fix conflicts"
   git push origin <your-branch-name>
   ```
3. We use next naming of branches:

| branch template                      | description                                                 |
| ------------------------------------ | ----------------------------------------------------------- |
| `feat/<short-feature-name>`          | new feature, ex. `feat-add-logger`                          |
| `fix/<short-fix-name>`               | fix of existing feature, ex. `fix-logger`                   |
| `refactor/<short-scope-description>` | refactor, linting, style changes, ex. `style-update-eslint` |
| `test/<short-scope-descriptiopn>`    | tests, ex. `test-db-connections`                            |
| `docs/<short-scope-descriptiopn>`    | documentation, ex. `test-db-connections`                    |

##### Important, before push

1. We use **eslint** with this [rules][eslint-rules] to lint code, before making pull
   request, lint your code:
   ```bash
   npm run lint
   ```
2. Before making pull request, run tests

   ```bash
   npm run test
   ```

[gitflow-docs]: https://gitversion.readthedocs.io/en/latest/git-branching-strategies/gitflow-examples/
[eslint-rules]: .eslintrc
