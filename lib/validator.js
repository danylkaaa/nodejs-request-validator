const Joi = require('@hapi/joi');

const DEFAULT_ERROR_STATUS_CODE = 423;
const DEFAULT_ERROR_CODE = 'INVALID_REQUEST';

const DEFAULT_ERROR_CONVERTER = (ctx, validationResult) => {
  ctx.body = {
    status: 'error',
    statusCode: DEFAULT_ERROR_STATUS_CODE,
    code: DEFAULT_ERROR_CODE,
    data: validationResult.errors
  };
  ctx.status = DEFAULT_ERROR_STATUS_CODE;
};
const DEFAULT_OPTIONS = {
  allowUnknown: true, // allows object to contain unknown keys which are ignored
  skipFunctions: false, // event is not supposed to have functions
  stripUnknown: true, // remove unknown elements from objects and arrays, if you need this behavior use .unknown()
  presence: 'required', // sets the default presence requirements: 'optional', 'required', or 'forbidden'
  abortEarly: true, // when true, stops validation on the first error, otherwise returns all the errors found. Defaults to true
};

/**
 * Validation middleware
 * @param {Object} joiSchema
 * @param {Object} options
 * @param {function(ctx, ValidationResult)} errorConverter
 * @returns {function(*, *)}
 */
function middleware(joiSchema, options = {}, errorConverter = DEFAULT_ERROR_CONVERTER) {
  options = Object.assign({ ...DEFAULT_OPTIONS }, options);
  joiSchema = Object.assign({
    body: Joi.object(),
    query: Joi.object(),
    params: Joi.object()
  }, joiSchema);
  return async (ctx, next) => {
    const inputData = {
      query: { ...ctx.request.query },
      body: { ...ctx.request.body },
      params: { ...ctx.params }
    };
    const validationResult = validate(inputData, joiSchema, options);
    if (!validationResult.ok) {
      if (typeof errorConverter === 'function') {
        errorConverter(ctx, validationResult);
      } else {
        DEFAULT_ERROR_CONVERTER(ctx, validationResult);
      }
      return;
    }
    ctx.request.query = validationResult.value.query;
    ctx.request.body = validationResult.value.body;
    ctx.request.params = validationResult.value.params;

    await next();
  };
}

/**
 * @typedef {object} c
 * @property {boolean} ok
 * @property {object} value
 * @property {object} errors
 */

/**
 * Validation method
 * @param {Object} data
 * @param {Object} joiSchema
 * @param {Object} options
 * @returns {ValidationResult}
 */
function validate(data, joiSchema, options = DEFAULT_OPTIONS) {
  options = Object.assign({ ...DEFAULT_OPTIONS }, options);
  const { error, value } = Joi.validate(data, joiSchema, options);
  if (!error) {
    return {
      ok: true,
      value
    };
  }

  const errors = (error.details || []).reduce((errorsDetails, next) => {
    errorsDetails[next.path.join('.')] = next.message;
    return errorsDetails;
  }, {});

  return {
    ok: false,
    errors
  };
}

module.exports = {
  validate,
  middleware
};
