const { expect } = require('chai');
const { validate, middleware } = require('../lib');
const Joi = require('joi');
const { fake } = require('sinon');

describe('LoadConfig', () => {
  describe('validate', () => {
    const SCHEMA = {
      body: Joi.object().keys({
        required: Joi.string(),
        optional: Joi.string().optional()
      })
    };
    it('expect validate object (valid)', () => {
      const result = validate({
          body: {
            required: 'hello'
          }
        },
        SCHEMA);
      expect(result).to.have.all.keys('ok', 'value');
      expect(result.ok).to.be.eq(true);
    });
    it('expect validate object (invalid)', () => {
      const result = validate({
          body: {
            optional: 'hello'
          }
        },
        SCHEMA);
      expect(result).to.have.all.keys('ok', 'errors');
      expect(result.ok).to.be.eq(false);
      expect(result.errors).to.be.an('object').and.have.all.keys('body.required');
    });
  });
  describe('middleware', () => {
    it('validate without options (valid body)', async () => {
      const cb = fake();
      const ctx = {
        request: {
          body: {
            required: 'Hello world',
            optional: 123,
          },
        },
      };
      await middleware({
        body: Joi.object().keys({
          required: Joi.string().required(),
          optional: Joi.number().optional(),
          forbidden: Joi.boolean().forbidden(),
        }),
      })(ctx, cb);
      expect(cb.called).equal(true);
    });
    it('validate without options (invalid body)', async () => {
      const cb = fake();
      const ctx = {
        request: {
          body: {
            required: 'Hello world',
            optional: 123,
            forbidden: true,
          },
        },
      };
      await middleware({
        body: Joi.object().keys({
          required: Joi.string().required(),
          optional: Joi.number().optional(),
          forbidden: Joi.boolean().forbidden(),
        }),
      })(ctx, cb);
      expect(ctx.body).to.deep.eq({
        statusCode: 423,
        status: 'error',
        code: 'INVALID_REQUEST',
        data: { 'body.forbidden': '"forbidden" is not allowed' },
      });
      expect(cb.called).equal(false);
    });
    it('validate with custom options (valid body)', async () => {
      const cb = fake();
      await middleware(
        {
          body: Joi.object().keys({
            required: Joi.string().required(),
            optional: Joi.number().optional(),
            forbidden: Joi.boolean().forbidden(),
          }),
        },
        {
          allowUnknown: false,
        }
      )(
        {
          request: {
            body: {
              required: 'Hello world',
              optional: 123,
            },
          },
        },
        cb
      );
      expect(cb.called).equal(true);
    });
    it('validate with custom options (invalid body)', async () => {
      const cb = fake();
      const ctx = {
        request: {
          body: {
            required: 'Hello world',
            optional: 123,
            unknown: true,
          },
        },
      };
      await middleware(
        {
          body: Joi.object().keys({
            required: Joi.string().required(),
            optional: Joi.number().optional(),
            forbidden: Joi.boolean().forbidden(),
          }),
        },
        {
          allowUnknown: false,
          stripUnknown: false,
        }
      )(ctx, cb);
      expect(cb.called).equal(false);
    });
  });
});
