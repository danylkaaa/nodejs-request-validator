declare module '@zulus/request-validator' {
    import Joi from "@hapi/joi";

    interface IValidationResult {
        ok: boolean;
        value?: T;
        errors: {
            [path: string]: string
        }
    }

    export interface RequestValidationSchema {
        body?: Joi.SchemaLike;
        query?: Joi.SchemaLike;
        params?: Joi.SchemaLike;
    }
    export interface IErrorConverter<T> {
        (ctx: Koa.Middleware<T>, validationResult: IValidationResult): any
    }
    export function validate<T>(
        data: T,
        joiSchema: Joi.SchemaLike,
        options?: Joi.ValidationOptions): IValidationResult

    export function middleware<T>(
        joiSchema: RequestValidationSchema,
        options?: Joi.ValidationOptions,
        errorConverter?: IErrorConverter<T>
    )
}